package com.example.vlajky

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiService {
    @GET("name/{countryName}?fullText=true")
    fun getCountryInfo(@Path("countryName") countryName: String): Call<List<CountryModel>>
}
