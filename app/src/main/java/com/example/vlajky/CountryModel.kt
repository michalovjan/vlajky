package com.example.vlajky

data class FlagsModel(
    val png: String? = null,
)

data class CountryModel(
    val flags: FlagsModel? = null,
)
