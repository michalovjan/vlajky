package com.example.vlajky

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.ListView
import androidx.navigation.fragment.findNavController

class HistoryFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_history, container, false)

        val preferences: SharedPreferences = requireActivity().getSharedPreferences("flagsAppData", Context.MODE_PRIVATE)
        val entries: List<String> = preferences.all.toList().map { "Country ${it.first} has flag on this url: ${it.second}" }
        val myListView = view.findViewById<ListView>(R.id.list)
        val arrayAdapter: ArrayAdapter<String> =
            ArrayAdapter(requireActivity(), android.R.layout.simple_list_item_1, entries)
        myListView.adapter = arrayAdapter

        val clearButton = view.findViewById<Button>(R.id.clear)
        clearButton.setOnClickListener {
            val editor = preferences.edit()
            editor.clear()
            editor.apply()
            arrayAdapter.clear()
        }

        val homeButton =  view.findViewById<Button>(R.id.homeButton)
        homeButton.setOnClickListener {
            findNavController().navigate(R.id.action_historyFragment_to_homeFragment)
        }
        return view
    }
}