package com.example.vlajky

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.google.android.material.textfield.TextInputEditText
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomeFragment : Fragment() {

    lateinit var countryFlagImage: ImageView
    var image_url: String = "https://upload.wikimedia.org/wikipedia/commons/thumb/5/50/Flag_with_question_mark.svg/2560px-Flag_with_question_mark.svg.png"
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_home, container, false)

        val historyButton =  view.findViewById<Button>(R.id.button)
        historyButton.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_historyFragment)
        }
        countryFlagImage = view.findViewById(R.id.imageView)
        Glide.with(this).load(image_url).into(countryFlagImage)
        val thisFragment = this

        val findButton =  view.findViewById<Button>(R.id.get_country)
        findButton.setOnClickListener {
            val countryEdit = view.findViewById<TextInputEditText>(R.id.inputText)
            val country = countryEdit.text.toString()
            if (country == "") {
                Glide.with(this).load(image_url).into(countryFlagImage)
            }
            else {
                val retrofit = Retrofit.Builder()
                    .baseUrl("https://restcountries.com/v3.1/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()

                val countryService = retrofit.create(ApiService::class.java)
                countryService.getCountryInfo(country).enqueue(object : Callback<List<CountryModel>> {
                    override fun onResponse(call: Call<List<CountryModel>>, response: Response<List<CountryModel>>) {
                        val countryList = response.body() ?: return

                        if (countryList.isNotEmpty()) {
                            val flagsPngUrl = countryList[0].flags?.png
                            Glide.with( thisFragment).load(flagsPngUrl).into(countryFlagImage)

                            val preferences: SharedPreferences = activity!!.getSharedPreferences("flagsAppData", Context.MODE_PRIVATE)
                            val editor = preferences.edit()
                            editor.putString(country, flagsPngUrl)
                            editor.apply()
                        }
                    }

                    override fun onFailure(call: Call<List<CountryModel>>, t: Throwable) {
                        Glide.with( thisFragment).load(image_url).into(countryFlagImage)
                    }
                })
            }
        }


        return view
    }
}